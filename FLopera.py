#!/usr/bin/python3
import serial
import time
import mido
import sys

ser_name = '/dev/ttyUSB0'
midi_file_name = "test.midi"
channels = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]

# Get command line args.
def parse_args():
    i = 0
    while i < len(sys.argv):
        if sys.argv[i] == "-h" or sys.argv[i] == "--help":
            print_floppy()
            print("FLoppera - 2020 Tristan Carlson")
            print("Options:")
            print("-s\tSpecify serial device to send notes to.")
            print("-f\tSpecify midi file")
            print("-c\tSpecify channels to send to floppy.")
            exit()
        if sys.argv[i] == "-s":
            global ser_name 
            ser_name = sys.argv[i+1]
            i += 2
            continue
        if sys.argv[i] == '-f':
            global midi_file_name 
            midi_file_name = sys.argv[i+1]
            i += 2
            continue

        if sys.argv[i] == '-c':
            global channels
            channels = sys.argv[i+1].split(",")
            for x in range(len(channels)):
                channels[x] = int(channels[x])
            i += 2
            continue

        i += 1



def load_midi():
    mid = mido.MidiFile(midi_file_name)
    return mid


# Handles playback and timing of midi.  simply waits specified time before
# sending notes to the arduino.
def play_midi(mid):
    for msg in mid:
        time.sleep(msg.time)
        if not msg.is_meta:
            send_note(msg)


# Send note over seial to Arduino.  
# Arduino takes notes as 2 bytes, byte0 = note, byte1 = on/off
def send_note(msg):
    if msg.type == 'note_on':
        if not(msg.channel in channels):
            return
        print(msg)
        ser.write(bytes([msg.note]) + b'\x01')

    if msg.type == 'note_off':
        if not(msg.channel in channels):
            return
        print(msg)
        ser.write(bytes([msg.note]) + b'\x00')
        pass


# Prints a floppy disk to screen because why not.
def print_floppy():
    print('-' * 38 + '\\')
    for y in range(16):
        print('|', end ='')
        if y == 0:
            print(" ^ |" + " " * 4 + '|' + " " * 10 + '|' + " " * 4 + "|" + " " *2 + "|" + " " * 10 +  "\\")
            
        elif y == 1:
            print(" | |" + " "* 4 + '|' + " " * 10 + '|' + " " * 4 + "|" + " " *2 + "|" + " " * 10 +  "|")
        elif y <= 3 and y >= 2:
            print("   |" + " "* 4 + '|' + " " * 10 + '|' + " " * 4 + "|" + " " *2 + "|" + " " * 10 +  "|")
        elif y == 4:
            print("   " + "-" * 25 + " " * 10 + "|"  )
        elif y == 6:
            print(" " * 3 + "-" * (38-6) +  " " * 3 + "|")
        elif y > 6 and y < 15:
            print(" " * 3 + "|" + " " * 30 + "|" + " " * 3 + "|")
        elif y == 15:
            print("[ ]|" + " " * 30  + "|[ ]|") 


        
        else:
            print(" " * 38 + "|")
    print('-' * 40)
    


def main():
    print_floppy()
    print("FLoppera - 2020 Tristan Carlson")

    parse_args()
    global ser
    ser = serial.Serial(ser_name)
    mid = load_midi()
    
    print("Loaded midi '{file}', with {num_tracks} tracks."
            .format(file=midi_file_name, num_tracks=len(mid.tracks)))

    print("Tracks:")
    for i, track in enumerate(mid.tracks):
        print("Track {}: {}".format(i, track))
    print("playing on channels: {}".format(channels))
    
    play_midi(mid)

main()
