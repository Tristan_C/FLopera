#define STEP_PIN 12
#define DIR_PIN 11
#define TRACK_0_PIN 10
int TIMEOUT = 4000; // In milis
int POS = 0;
char DIR = 1;
char STEP_STATE = LOW;
char DIR_STATE = LOW;
volatile struct CMD* CURRENT_CMD = NULL;
// Array of MIDI  note frequencies.  The data at each index corresponds to the frequency of the midi note with that index number.
const float NOTES[128] = { /* 0 */ 8.18, /* 1 */ 8.66, /* 2 */ 9.18, /* 3 */ 9.72, 
                          /* 4 */ 10.30, /* 5 */ 10.91, /* 6 */ 11.56, /* 7 */ 12.25, 
                          /* 8 */ 12.98, /* 9 */ 13.75, /* 10 */ 14.57, /* 11 */ 15.43, 
                          /* 12 */ 16.35, /* 13 */ 17.32, /* 14 */ 18.35, /* 15 */ 19.45, 
                          /* 16 */ 20.60, /* 17 */ 21.83, /* 18 */ 23.12, /* 19 */ 24.50, 
                          /* 20 */ 25.96, /* 21 */ 27.50, /* 22 */ 29.14, /* 23 */ 30.87, 
                          /* 24 */ 32.70, /* 25 */ 34.65, /* 26 */ 36.71, /* 27 */ 38.89, 
                          /* 28 */ 41.20, /* 29 */ 43.65, /* 30 */ 46.25, /* 31 */ 49.00, 
                          /* 32 */ 51.91, /* 33 */ 55.00, /* 34 */ 58.27, /* 35 */ 61.74, 
                          /* 36 */ 65.41, /* 37 */ 69.30, /* 38 */ 73.42, /* 39 */ 77.78, 
                          /* 40 */ 82.41, /* 41 */ 87.31, /* 42 */ 92.50, /* 43 */ 98.00, 
                          /* 44 */ 103.83, /* 45 */ 110.00, /* 46 */ 116.54, /* 47 */ 123.47, 
                          /* 48 */ 130.81, /* 49 */ 138.59, /* 50 */ 146.83, /* 51 */ 155.56, 
                          /* 52 */ 164.81, /* 53 */ 174.61, /* 54 */ 185.00, /* 55 */ 196.00, 
                          /* 56 */ 207.65, /* 57 */ 220.00, /* 58 */ 233.08, /* 59 */ 246.94, 
                          /* 60 */ 261.63, /* 61 */ 277.18, /* 62 */ 293.66, /* 63 */ 311.13, 
                          /* 64 */ 329.63, /* 65 */ 349.23, /* 66 */ 369.99, /* 67 */ 392.00, 
                          /* 68 */ 415.30, /* 69 */ 440.00, /* 70 */ 466.16, /* 71 */ 493.88, 
                          /* 72 */ 523.25, /* 73 */ 554.37, /* 74 */ 587.33, /* 75 */ 622.25, 
                          /* 76 */ 659.26, /* 77 */ 698.46, /* 78 */ 739.99, /* 79 */ 783.99, 
                          /* 80 */ 830.61, /* 81 */ 880.00, /* 82 */ 932.33, /* 83 */ 987.77, 
                          /* 84 */ 1046.50, /* 85 */ 1108.73, /* 86 */ 1174.66, /* 87 */ 1244.51, 
                          /* 88 */ 1318.51, /* 89 */ 1396.91, /* 90 */ 1479.98, /* 91 */ 1567.98, 
                          /* 92 */ 1661.22, /* 93 */ 1760.00, /* 94 */ 1864.66, /* 95 */ 1975.53, 
                          /* 96 */ 2093.00, /* 97 */ 2217.46, /* 98 */ 2349.32, /* 99 */ 2489.02, 
                          /* 100 */ 2637.02, /* 101 */ 2793.83, /* 102 */ 2959.96, /* 103 */ 3135.96, 
                          /* 104 */ 3322.44, /* 105 */ 3520.00, /* 106 */ 3729.31, /* 107 */ 3951.07, 
                          /* 108 */ 4186.01, /* 109 */ 4434.92, /* 110 */ 4698.64, /* 111 */ 4978.03, 
                          /* 112 */ 5274.04, /* 113 */ 5587.65, /* 114 */ 5919.91, /* 115 */ 6271.93, 
                          /* 116 */ 6644.88, /* 117 */ 7040.00, /* 118 */ 7458.62, /* 119 */ 7902.13, 
                          /* 120 */ 8372.02, /* 121 */ 8869.84, /* 122 */ 9397.27, /* 123 */ 9956.06, 
                          /* 124 */ 10548.08, /* 125 */ 11175.30, /* 126 */ 11839.82, /* 127 */ 12543.85, 
 };

float NOTE_HALF_PERIOD[128];

struct CMD{
  unsigned char note;    
  unsigned char on;
  
};

/* Recieve serial data */
void recv_data(){
  if (Serial.available() < 1){
    return;
  }
  unsigned long start_t = millis();
  static struct CMD ret;
  unsigned char buf[2];
  int i = 0;
  
  while(i < 2){
    // Check if this is taking too long.
    if (millis() - start_t >= TIMEOUT){
      for (int i = 0; i < Serial.available(); i++){
        Serial.read();    // Throw out buffer
      }
      return;
    }
    // read available serial data.
    if (Serial.available() > 0){
      buf[i] = Serial.read();
      i += 1;
    }  
  }

  // Cast data to a CMD struct + set global pointer to it.
  ret = *((struct CMD *) &buf);
  CURRENT_CMD = &ret;
}

/* Sets direction of floppy head */
void set_dir(char dir){
  if (dir > 0){
    digitalWrite(DIR_PIN, HIGH);
    DIR_STATE = HIGH;
    DIR = 1;
    }
   else{
     digitalWrite(DIR_PIN, LOW);
     DIR_STATE = LOW;
     DIR = -1;
   }
}


/* Move floppy head.  This fn assumes that bon startup the STEP pin is HIGH.
 * The floppy only moves it's head when it sees the step pin transition from HIGH to LOW.  
 * This function only TOGGLES the step pin, so every 2nd call will result in an actual movement of the head.*/
void move_head(){
  static unsigned int i = 0;
  static char movement = DIR;

  // Ensure the head is 'in bounds'
  if (POS > 70){
    set_dir(-1);
    movement = DIR;
  }
  if (POS < 10){
    set_dir(1);
    movement = DIR;
  }
  
  // toggle the step pin.
   STEP_STATE = ! STEP_STATE;
   digitalWrite(STEP_PIN, STEP_STATE);

  // Every second step the head actually moves.  Increment nessesary info.
   if (i % 2 == 0){
    POS += DIR;
    set_dir(DIR * -1);
   }
   
   // Option to make the head actually scroll. (Currently disabled)
   if (0){
    set_dir(movement);
   }
   i += 1;
}

/* Plays note recieved over serial.  We must call move head twice in order for the head to move, thus we only 
 * wait half of the period.*/
float play_note(){
  // Check if note is asking for on or off.
   if (! CURRENT_CMD->on){
    return 0;
   }
   // Get wait time now, in case we recieve a new command while moving head.
    float wait = NOTE_HALF_PERIOD[CURRENT_CMD->note];
    move_head();
    return wait;
    
 }


// Called at start by arduino 'magic'
void setup() {
  Serial.begin(9600);
  pinMode(DIR_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(TRACK_0_PIN, INPUT);
  digitalWrite(DIR_PIN, HIGH);
  DIR_STATE = HIGH;
  digitalWrite(STEP_PIN, HIGH);
  STEP_STATE = HIGH;

  // Get the floppy head to a known position.
  Serial.println("Finding track 0...");
  
  while(digitalRead(TRACK_0_PIN)){
    STEP_STATE = ! STEP_STATE;
    digitalWrite(STEP_PIN, STEP_STATE);
    delay(10);
  }
  
  Serial.println("done.");

  // Move head 10 steps in.
  digitalWrite(DIR_PIN, LOW);
  DIR_STATE = LOW;
  for(int i =0; i < 20; i++){
    STEP_STATE = ! STEP_STATE;
    digitalWrite(STEP_PIN, STEP_STATE);
    delay(10);
  }

  // Set step state high, as move_head assumes it recieves the step pin HIGH on start.
  STEP_STATE = HIGH;
  digitalWrite(STEP_PIN, STEP_STATE);

  POS = 70;
  DIR = -1;


  // Calculate half period now.  This saves us wasting time doing this at runtime.
  // Yes I could have simply used this array and not bothered with the NOTES array, but I'm Lazy, and this was easier.
  for (int i = 0; i < 128; i++){
    NOTE_HALF_PERIOD[i] = (1/NOTES[i]) / 2 * (1000000);    // gives 1/2 of period in micros.
  }

  Serial.print("done init");

}

// Called repeatedly by arduino 'magic'
void loop() {

  static float wait = 0;
  static unsigned long prev_t = 0;
  recv_data();
  // Keep exiting (and consiquently starting) this fn if either: we dont have a command, or not enough time has passed
  if (CURRENT_CMD == NULL || micros() - prev_t < wait){
    return;
  }
 
  wait = play_note();
  prev_t = micros();
  
}
