#!/usr/bin/python3
"""
Parse table of midi notes and frequencies and output C array to output.txt

"""


def main():
    
    # Read input into memory.
    with open('notes.txt', 'r') as f:
        buffer = f.read()

    # Split file into a list delineated by newline.
    lines = buffer.split('\n')

    to_rem = []
    i = 0
    # Split each line by tabs.
    for s in lines:
        if not s:
            to_rem.append(i)
        lines[i] = s.split('\t')
        i += 1

    # Remove blank lines.
    for r in to_rem:
        del(lines[r])

    # Format parsed info into C array in format {/* note */ freq, ...}
    output = "{ "
    if lines[0][0] != "0":
        lines.reverse()
    n = 0
    for info in lines:
        output += "/* " + info[0] + " */ " + info[5] + ", "
        n += 1
        if n % 4 == 0:
            output += ("\n")

    output += " }"

    # Write to disk.
    with open("output.txt", 'w') as f:
        f.write(output)
    print("found {} notes".format(len(lines)))

main()

